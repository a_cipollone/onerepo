﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sandrino.Core.Models
{
    class Anagrafica
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Cognome { get; set; }
        public string Indirizzo { get; set; }

    }
}
